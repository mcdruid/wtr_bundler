#!/bin/bash

tmp_dir=$(mktemp -d -t wtr_bundler-XXXXXXXXXX)
cd $tmp_dir

echo "# Working in $tmp_dir"

curl -sO https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod a+x wp-cli.phar

curl --create-dirs -so add/vendor/wp-cli/entity-command/wapuu-the-ripper/command.php https://raw.githubusercontent.com/mcdruid/wapuu-the-ripper/master/command.php
curl -so add/vendor/wp-cli/entity-command/wapuu-the-ripper/wordlist.txt https://raw.githubusercontent.com/mcdruid/wapuu-the-ripper/master/wordlist.txt

# grab a clean copy of entity-command.php
phar extract -f wp-cli.phar -i entity-command.php add

# ..and hack the wtr include into it
echo -e "\ninclude_once __DIR__ . '/wapuu-the-ripper/command.php';\n" >> add/vendor/wp-cli/entity-command/entity-command.php 

phar add -f wp-cli.phar add

cd -
cp -av $tmp_dir/wp-cli.phar .
if [ -d "$tmp_dir" ]
then
  echo "# Cleaning up $tmp_dir"
  rm -rf $tmp_dir
 fi

echo "# Updated wp-cli.phar:"
./wp-cli.phar --info | tail -n1
./wp-cli.phar help | grep -w wtr
